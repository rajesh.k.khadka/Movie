import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography'
import ClippedDrawer from './AppDrawer' 
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <ClippedDrawer></ClippedDrawer> 
      </div>
    );
  }
}

export default App;
